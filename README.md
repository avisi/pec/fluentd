# Fluentd Docker with http plugin

This repository contains a simple dockerfile which adds the fluent-plugin-out-http plugin to a fluentd install.

[![pipeline status](https://gitlab.com/avisi/pec/fluentd/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/fluentd/commits/master)

---

## Table of Contents

- [Contribute](#contribute)
- [Automated build](#automated-build)
- [License](#license)

## Contribute

All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/fluentd/issues).

## Automated build

This image is build at least once a month automatically. All PR's are automatically build.

## License

[MIT © Avisi B.V.](LICENSE)
